# Eastern Skaftá cauldron GLOF simulation, Iceland

**SC leader:** <img src="https://gitlab.com/cheese5311126/CHEESE/-/raw/main/Logos/IMO.png?inline=false" width="80">

**Partners:** <img src="https://gitlab.com/cheese5311126/CHEESE/-/raw/main/Logos/CSC.png?inline=false" width="150">

**Codes:** [ELMER/ICE](https://gitlab.com/cheese5311126/codes/elmer_ice/elmer_ice)

**Target EuroHPC Architectures:** LUMI-C, MAHTI

**Type:** Hazard assessment

## Description

The model will simulate rapidly-rising glacier lake outburst floods (jökulhlaups)
from the Eastern Skaftá Cauldron underneath the W-Vatnajökull ice cap in
Iceland. These floods travel ~40 km underneath ~400-m thick glacier and
emerge in the Skaftá river at the terminus of Skaftárjökull outlet glacier where
they endanger roads, communication lines and farmland. The jökulhlaups create
a flood path along the glacier bed by lifting the glacier and melting a conduit into
the ice. The jökulhlaup model is implemented as a two-dimensional horizontal
model for the subglacial water-flux/pressure distribution and a fully
three-dimensional visco-elastic model describing the glacier deformation in
reaction to the pressurisation by the subglacial flood. The model will be a step
towards better understanding of glacier lake outburst floods, which include some
of the largest documented floods on Earth, and are among the most important
geomorphological processes in watersheds downstream from glaciers. They are
societally important in many areas because of the associated flood hazard.

<img src="SC9.1.png" width="600">

**Figure 3.14.1.** Graphical abstract of SC9.1 showing the modelled visco-elastic response
of a glacier to a prescribed propagating pressure disturbance in the subglacial hydraulic
system.

## Expected results

There exists a wider interest in the glaciological community on glacier hazard modeling, in particular on
glacier outburst floods (GLOFs). Jökulhlaups/GLOFs can lead to flood hazards in areas close to glaciers, so improved
scientific understanding is relevant for hazard management in Iceland and several other countries such as Norway, Greenland
and regions in the Himalayas.